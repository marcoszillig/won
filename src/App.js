import React from 'react';
import './App.scss';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/home/Home'
import SignIn from './components/auth/SignIn'
import SignUp from './components/auth/SignUp'
import Workouts from './components/workouts/Workouts'
import CreateWorkout from './components/workouts/CreateWorkout'
import Navbar from './components/layout/Navbar';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/signin' component={SignIn} />
          {/* <Route path='/signup' component={SignUp} /> */}
          <Route path='/workouts' component={Workouts} />
          <Route path='/create' component={CreateWorkout} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
