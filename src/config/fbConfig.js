import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyA35BCatkcFgHHlzyhEhltYwjH_MBBPrG0",
  authDomain: "zillig-won.firebaseapp.com",
  databaseURL: "https://zillig-won.firebaseio.com",
  projectId: "zillig-won",
  storageBucket: "zillig-won.appspot.com",
  messagingSenderId: "996952205487",
  appId: "1:996952205487:web:6a69f80019f50342"
};

firebase.initializeApp(firebaseConfig);
// firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase;
