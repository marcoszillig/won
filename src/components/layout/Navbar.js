import React, { Component } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import SignedInLinks from './SignedInLinks'
import SignedOutLinks from './SignedOutLinks'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'

class NavBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      condition: false
    }

  }
  handleResize = (e) => {
    this.setState({ width: window.innerWidth })
  }

  handleClick = () => {
    this.setState({
      condition: !this.state.condition
    });    
  }

  handleMenu = () => {
    document.querySelectorAll('.menu__icon__lines')[1].classList.toggle('deactivated')
    document.querySelectorAll('.menu__icon__lines')[0].classList.toggle('line-a')
    document.querySelectorAll('.menu__icon__lines')[2].classList.toggle('line-b')
    document.querySelector('.won__navbar').classList.toggle('active')
  }
  componentDidMount() {
    window.addEventListener('resize', this.handleResize)           
  }
   
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }
  toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    }
  
  render() {   
    const { auth, profile } = this.props
    // const width = this.state.width
    const links = auth.uid ? <SignedInLinks profile={profile}/> : <SignedOutLinks />
    return (
      <div className="won__navbar">
        <div className="menu__icon" onClick={this.handleMenu}>
          <span className="menu__icon__lines"></span>
          <span className="menu__icon__lines" data-src='1'></span>
          <span className="menu__icon__lines"></span>
        </div>
        <div className="won__navbar__overlay">
          <NavLink to="/">Home</NavLink>
          <NavLink to="/workouts">Workouts</NavLink>
          <NavLink to="#link">About</NavLink>
          <NavLink to="#link">Contact</NavLink>
        </div>
        {/* <div className= { this.state.condition ? "sidenav active" : "sidenav" } id='sidenav_overlay' onClick={this.handleClick}>
          {links}
        </div> */}
      </div>
      
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile
  }
}

export default connect(mapStateToProps)(NavBar)