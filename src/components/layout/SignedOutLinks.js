import React from 'react'
import { NavLink } from 'react-router-dom'
import Button from 'react-bootstrap/Button'

const SignedOutLinks = () => {
  return (
    <ul className="signed-out__links">
      <li>
        <NavLink to='/signup' className="">
          <Button>Signup</Button>
        </NavLink>
      </li>
      {/* <li>
        <NavLink to='/signin' className="">
          <Button>Login</Button>
        </NavLink>
      </li> */}
    </ul>
  )
}

export default SignedOutLinks 