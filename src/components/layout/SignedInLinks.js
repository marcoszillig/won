/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { signOut } from '../../store/actions/authActions'
import Button from 'react-bootstrap/Button'

const SignedInLinks = (props) => { 
  return (
    <Button>
      <a onClick={props.signOut}>Logout</a>
    </Button>
    
  )  
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default connect(null, mapDispatchToProps)(SignedInLinks) 