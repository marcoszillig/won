import React from 'react'
import { Link } from 'react-router-dom'

const AddButton = (props) => {
  return (
    <div className="workout__add__btn">
      <Link to='/create'>
        <button>Adicionar</button>
      </Link>
    </div>

  )
}


export default AddButton