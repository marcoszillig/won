import React from 'react'
import WorkoutCard from './WorkoutCard'

const WorkoutList = ({workouts}) => {
  return (
    <div className='cards'>
      { workouts && workouts.map( workout => {
        return (
          <WorkoutCard
            workout={workout}
            key={workout.id}
          />
        )
      })}
    </div>
  )
}


export default WorkoutList