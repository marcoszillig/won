import React, { Component } from 'react';
import { connect } from 'react-redux'
import { createWorkout } from '../../store/actions/workoutActions'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import list from '../../data/exercise_list.json'
import { Link } from 'react-router-dom'

class CreateWorkout extends Component {
  state = {
    exercise: '',
    sets: '',
    reps: '',
    weight: '',
    time: '',
    type: '',
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.createWorkout(this.state)
    this.props.history.push('/workouts')
  }

  render() {
    const listOption = list.exercises.map(el => <option key={el.id}>{el.exercise}</option>)
    return (
      <div className='create__workout'>
        <div>

        <h2 className="workout__header">Create Workout</h2>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="exercise">
            <Form.Label>Exercício</Form.Label>
            <Form.Control type="text" onChange={this.handleChange}/>
          </Form.Group>
          <Form.Row>
            <Form.Group as={Col} controlId="sets">
              <Form.Label>Série</Form.Label>
              <Form.Control type="number" onChange={this.handleChange}/>
            </Form.Group>
            <Form.Group as={Col} controlId="reps">
              <Form.Label>Repetições</Form.Label>
              <Form.Control type="number" onChange={this.handleChange}/>
            </Form.Group>
            <Form.Group as={Col} controlId="weight">
              <Form.Label>Carga</Form.Label>
              <Form.Control type="number" onChange={this.handleChange}/>
            </Form.Group>
          </Form.Row>
          <Form.Group controlId="type">
            <Form.Label>Treino</Form.Label>
            <Form.Control as="select" onChange={this.handleChange}>
            <option>Selecione o seu treino</option>
              <option>a</option>
              <option>b</option>
              <option>c</option>
              <option>d</option>
            </Form.Control>
          </Form.Group>
          {/* <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Example textarea</Form.Label>
            <Form.Control as="textarea" rows="3" />
          </Form.Group>   */}
          <div className="btn__group">
            <Link to={'/workouts'}>
              <Button variant="danger">
                &#10005;
              </Button>
            </Link>
            <Button variant="primary" type="submit">
              &#9989;
            </Button>

          </div>
        </Form>
        </div>
      </div>
    )

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createWorkout: (workout) => dispatch(createWorkout(workout))
  }
}

export default connect(null, mapDispatchToProps)(CreateWorkout)