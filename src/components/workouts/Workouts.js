import React, { Component } from 'react';
import '../../App.scss';
import WorkoutList from './WorkoutList'
import AddButton from '../layout/AddButton'
// import FilterWorkout from './FilterWorkout'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import Button from '@material-ui/core/Button'


class Workouts extends Component {
  state = {
    show: false,
    type: '',
    filter_clear: true
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    const workoutHeader = document.querySelector('.workout__header')
    const filter = document.querySelector('.filter')

    workoutHeader.style.opacity = 1 - window.scrollY / 100
    filter.style.opacity = 1 - window.scrollY / 100
    
  }

  updateWorkoutHandler = (event, id) => {
    const workoutIndex = this.state.workouts.findIndex( w => {
      return w.id === id
    })

    const workout = {
      ...this.state.workouts[workoutIndex]
    }

    const workouts = [...this.state.workout]
    workouts[workoutIndex] = workout

    this.setState({workouts: workouts})
  }


  deleteWorkoutHandler = (workoutIndex) => {
    const workouts = [...this.state.workouts]
    workouts.splice(workoutIndex, 1)
    this.setState({workouts: workouts})
  }

  showModal = () => {
    const showModal = this.state.show
    this.setState({ show: !showModal })

  }

  hideModal = () => {
    this.setState({ show: false })
  }

  filter = (e) => {
    this.setState({
      type: e.currentTarget.dataset.filter,
      filter_clear: false
    })
    document.querySelector('.filter-btn-clear').classList.add('active')
    document.querySelector('.filter-btn-clear').style.display = 'block'
  }
  cleanFilter = (e) => {
    document.querySelector('.filter-btn-clear').style.display = 'none'
    document.querySelector('.filter-btn-clear').classList.remove('active')

    this.setState({
      type: '',
      filter_clear: true
    })
  }

  render() {
    const { workouts } = this.props
    console.log(this.state.type)
    let cards
    // let cards = workouts && workouts.filter( card => { return card.type === this.state.type });

    (this.state.type === '') ?  ( cards = workouts) : ( cards = workouts.filter( card => { return card.type === this.state.type }) )

    return (
      <div className='won__dashboard'>
        <div className='sidebar'>
          <div className="sidebar__logo">
            <a href="/workouts">
              <span>WoN</span>
            </a>
          </div>
          <div className='user-profile'><span>MZ</span> Marcos Zillig</div>
          <div className="filter">
            <h3 className="filter__title">Filtrar</h3>
            <div className="filter__group">
              <button className='filter-btn' data-filter="a" onClick={this.filter.bind(this)}>A</button>
              <button className='filter-btn' data-filter="b" onClick={this.filter.bind(this)}>B</button>
              <button className='filter-btn' data-filter="c" onClick={this.filter.bind(this)}>C</button>
              <button className='filter-btn-clear' data-filter="clear" disabled={this.state.filter_clear} onClick={this.cleanFilter.bind(this)}>Limpar filtros</button>
            </div>
          </div>
          <AddButton />
        </div>        
        <div className='workouts'>
          <h2 className="workout__header">Workouts</h2>
          <div className='workout-row'>
            <div className="workout__column">
              <WorkoutList workouts={cards}/>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  // console.log(state)
  return {
    workouts: state.firestore.ordered.workouts
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'workouts', orderBy: ['type', 'asc'] }
  ])
)(Workouts)