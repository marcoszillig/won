import React from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Badge from 'react-bootstrap/Badge'

const WorkoutCard = ({workout}) => {
  return (
    <Card.Body className='workout__card'>
      <Card.Title>{workout.exercise}</Card.Title>      
      <ul className='training-details'>
        <li className='training-detail--set'>
          <span>{workout.sets}</span>
          <i className="fas fa-times"></i>
          <span>{workout.reps} </span>
        </li>
        <li className='training-detail--weight'>
          <i className="fas fa-weight-hanging"></i>
          <span>{workout.weight}kgs</span>
        </li>
        {/* {workout.time.length > 0 
          ? <li className='training-detail--time'>
              <i className="fas fa-clock"></i>
              <span>{workout.time}'s</span>
            </li> 
          : null }             */}
        <li className='training-detail--type'>
            {workout.type}
          {/* <Badge pill variant="light">
          </Badge> */}
        </li>
      </ul> 
      {/* <div className='btn-group'>
        <Button
          className='btn-edit'
          variant="link">
          <i className="fas fa-edit"></i>
        </Button>
        <Button 
          className='btn-delete'
          variant="link"
          onClick={workout.click}>
          <i className="fas fa-times"></i>
        </Button>
      </div> */}
    </Card.Body>

  )
}

export default WorkoutCard