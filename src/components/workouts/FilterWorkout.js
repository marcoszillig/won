import React from 'react'
import Button from 'react-bootstrap/Button'

const SortWorkout = (props) => {

  let woType = props.workouts
  // props.workouts.type.filter( el => el.type)
  console.log(woType)
  // [...new Set()]
  if (props.workouts) {
    return(
      <Button
        variant="secondary"
        size="sm"
        onClick={props.click}>
        {props.workouts.type}
      </Button>
    )
  }
  return (
    <div className='loader'>&#x21bb;</div>
  )
}

export default SortWorkout