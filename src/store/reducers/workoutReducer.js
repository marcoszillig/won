const initState = {
  workouts : [
    { id: '0', exercise: 'Supino Reto', sets: 4, reps: 10, weight: 10, time: '', type: 'b'},
    { id: '1', exercise: 'Triceps polia alta', sets: 4, reps: 10, weight: 20, time: '', type: 'a'},
    { id: '2', exercise: 'Elevação Lateral', sets: 3, reps: 12, weight: 8, time: '40', type: 'c' },
    { id: '3', exercise: 'Supino Reto', sets: 4, reps: 10, weight: 10, time: '', type: 'b'},
    { id: '4', exercise: 'Triceps polia alta', sets: 4, reps: 10, weight: 20, time: '', type: 'a'},
    { id: '5', exercise: 'Elevação Lateral', sets: 3, reps: 12, weight: 8, time: '40', type: 'c' },
  ]
}

const workoutReducer = (state = initState, action) => {
  switch(action.type) {
    case 'CREATE_WORKOUT': 
      console.log('created workout', action.workout)
      return state
    case 'CREATE_PROJECT_ERROR':
      console.log('create project error', action.err)
      return state
      default:
      return state
  }
}

export default workoutReducer