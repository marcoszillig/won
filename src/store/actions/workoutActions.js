export const createWorkout = (workout) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    // make async call to database
    const firestore = getFirestore()
    firestore.collection('workouts').add({
      ...workout,
      userFirstName: 'Marcos',
      userLastName: 'Zillig',
      userId: 1244,
      createdAt: new Date()
    }).then(() => {
      dispatch({ type: 'CREATE_WORKOUT', workout})
    }).catch((err) => {
      dispatch({ type: 'CREATE_PROJECT_ERROR', err})
    })
  }
}